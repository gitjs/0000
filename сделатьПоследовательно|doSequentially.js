/*
#mdru/Функции/сделатьПоследовательно
#mdru 
#mdru Функция `сделатьПоследовательно(мир, список, функция, откликУспех, откликПровал = null)`
#mdru позволяет исполнить последовательно функцию следующего вида:
#mdru `функция(мир, список, номер, откликУспех, откликПровал)`.
#mdru 
#mdru В частности функция `сделатьПоследовательно()` используется для
#mdru загрузки файлов модулей ГитЖС.
#mdru 

//mden/Functions/doSequentially
//mden 
//mden `doSequentially(world, list, func, callbackSuccess, callbackFailure = null)`
//mden function may execute sequentially a function that looks like this:
//mden `func(world, list, id, callbackSuccess, callbackFailure)`.
//mden 
//mden Specifically, `doSequentially()` function is used to download GitJS
//mden module files.
//mden 
*/

function сделатьПоследовательно(мир, список, функция, откликУспех, откликПровал = null)
{
    var номер = 0;
    функция(мир, список, номер, продолжить, откликПровал);

    function продолжить()
    {
        номер += 1;
        if (номер < список.length)
        {
            функция(мир, список, номер, продолжить, откликПровал);
        }
        else
        {
            откликУспех();
        }
    }
}
