//MDRU # Класс `Модули`
//MDRU 
//MDRU Позволяет управлять модулями GitJS:
//MDRU 
//MDRU * загружать модуль по сети (HTTP) или из кэша браузера (IndexedDB);
//MDRU * сохранять изменения модуля в кэш браузера.
//MDRU 
//MDRU Экземпляр класса `Модули` создаётся пусковым скриптом `0000.js`
//MDRU и доступен как `мир.модули`.
//MDRU 

//MDEN 
//MDEN # `Modules` class
//MDEN
//MDEN Allows you to manage GitJS modules:
//MDEN 
//MDEN * load a module over network (HTTP) or from the browser cache (IndexedDB);
//MDEN * save changes of a module into browser cache.
//MDEN 
//MDEN An instance of `Modules` class is created by `0000.js` startup script
//MDEN and is available as `world.modules`.
//MDEN 

function Модули(ключ, мир)
{
    //MDRU 
    //MDRU ## Метод `Модули.использовать(указатели)`
    //MDRU 
    //MDRU Делает следующее с предоставленным списком указателей на модули:
    //MDRU 
    //MDRU * проверяет наличие всех запрошенных модулей в кэше;
    //MDRU * загружает по сети (HTTP) отсутствующие в кэше модули;
    //MDRU * сохраняет в кэше загруженные по сети модули;
    //MDRU * исполняет с помощью `eval()` у каждого модуля файлы `.js` с флагом `исполнить`;
    //MDRU * исполняет у каждого модуля файлы `.череда` в случае наличия файла-близнеца с расширением `.js` и флагом `исполнить`.
    //MDRU 
    //MDRU Для отслеживания результата использования следует подписаться на следующие
    //MDRU экземпляры класса `Уведомитель`:
    //MDRU 
    //MDRU * `мир.модули.использовали` для отслеживания успешного использования;
    //MDRU * `мир.модули.неИспользовали` для отслеживания неудачного использования;
    //MDRU * `мир.модули.неСохранили` для отслеживания неудачного сохранения в кэш.
    //MDRU 
    //MDRU Пример:
    //MDRU 
    //MDRU ```js
    //MDRU мир.модули.использовать([
    //MDRU     "https://bitbucket.org/gitjs/privet-hello/raw/master/0000",
    //MDRU     "http://localhost:40000/gitjs/proverit-test-0000/0000",
    //MDRU ]);
    //MDRU 
    //MDRU // Успешное использование.
    //MDRU мир.модули.использовали.подписатьРаз(function() {
    //MDRU     console.debug("Модули использованы. Поехали!");
    //MDRU });
    //MDRU 
    //MDRU // Неудачное использование.
    //MDRU мир.модули.неИспользовали.подписатьРаз(function() {
    //MDRU     console.error(`ОШИБКА Не удалось использовать модули: '${мир.модули.ошибка}'`);
    //MDRU });
    //MDRU 
    //MDRU // Неудачное сохранение.
    //MDRU мир.модули.неСохранили.подписатьРаз(function() {
    //MDRU     console.error(`ОШИБКА Не удалось сохранить модули: '${мир.модули.ошибка}'`);
    //MDRU });
    //MDRU ```
    //MDRU 

    //MDEN 
    //MDEN ## `Modules.use(pointers)` method
    //MDEN 
    //MDEN Performs the following actions with the provided pointers to modules:
    //MDEN 
    //MDEN * looks up the modules in the cache;
    //MDEN * loads modules over network (HTTP) if they are not not present in the cache;
    //MDEN * saves loaded modules into the cache;
    //MDEN * executes each `.js` file having `исполнить` (`execute`) flag with the help of `eval()`;
    //MDEN * executes each `.череда` (sequence) file if it has a twin `.js` file with `исполнить` (`execute`) flag set.
    //MDEN 
    //MDEN To track the status of `use()` execution you should subscribe to one of
    //MDEN the following instances of `Reporter` class:
    //MDEN 
    //MDEN * `world.modules.used` to track successful usage;
    //MDEN * `world.modules.notUsed` to track failed usage;
    //MDEN * `world.modules.notSaved` to track failed caching.
    //MDEN 
    //MDEN Example:
    //MDEN 
    //MDEN ```js
    //MDEN world.modules.use([
    //MDEN     "https://bitbucket.org/gitjs/privet-hello/raw/master/0000",
    //MDEN     "http://localhost:40000/gitjs/proverit-test-0000/0000",
    //MDEN ]);
    //MDEN 
    //MDEN // Successful usage.
    //MDEN world.modules.used.subscribeOnce(function() {
    //MDEN     console.debug("Modules were used. Let's go!");
    //MDEN });
    //MDEN 
    //MDEN // Failed usage.
    //MDEN world.modules.notUsed.subscribeOnce(function() {
    //MDEN     console.error(`ERROR Could not use modules: '${world.modules.error}'`);
    //MDEN });
    //MDEN 
    //MDEN // Failed saving.
    //MDEN world.modules.notSaved.subscribeOnce(function() {
    //MDEN     console.error(`ERROR Could not save modules: '${world.modules.error}'`);
    //MDEN });
    //MDEN ```
    //MDEN 

    this.использовали = new мир.Уведомитель();
    this.неИспользовали = new мир.Уведомитель();
    this.неСохранили = new мир.Уведомитель();

    this.used = this.использовали;
    this.notUsed = this.неИспользовали;
    this.notSaved = this.неСохранили;

    this.использовать = function(указатели)
    {
        расшифрованныеУказатели = this._расшифроватьУказатели(указатели);
        this.исп.используемыеУказатели = расшифрованныеУказатели.slice();
        this.исп.указатели = расшифрованныеУказатели.slice();
        this.исп.уведомить("использовать");
        this.исп.тут = this;
    };

    this.use = this.использовать;


    // НАДО Описать MDRU/EN
    this.отладили = new мир.Уведомитель();
    this.отладка = null;

    this.debugged = this.отладили;
    this.debug = null;

    // НАДО Описать MDRU/EN
    this.модульПоИмени = function(имя)
    {
        for (var указатель in this.модули)
        {
            var модуль = this.модули[указатель];
            if (модуль.имя == имя)
            {
                return модуль;
            }
        }

        return null;
    };
    this.moduleByName = this.модульПоИмени;

    // НАДО Описать MDRU/EN
    this.модульПоУказателю = function(указатель)
    {
        return this.модули[указатель];
    };
    this.moduleByPointer = this.модульПоУказателю;

    //mdru/Модули/загрузилиСодержимоеСохранённогоМодуля
    //mdru 
    //mdru Уведомление об успешном исполнении метода
    //mdru `загрузитьСодержимоеСохранённогоМодуля`.
    //mdru 
    this.загрузилиСодержимоеСохранённогоМодуля = new мир.Уведомитель();

    //mden/Modules/loadedSavedModuleContents
    //mden 
    //mden Notification of a successful completion of
    //mden `loadedSavedModuleContents` method.
    //mden 
    this.loadedSavedModuleContents = this.загрузилиСодержимоеСохранённогоМодуля;

    //mdru/Модули/неЗагрузилиСодержимоеСохранённогоМодуля
    //mdru 
    //mdru Уведомление о неудачном исполнении метода
    //mdru `загрузитьСодержимоеСохранённогоМодуля`.
    //mdru 
    this.неЗагрузилиСодержимоеСохранённогоМодуля = new мир.Уведомитель();

    //mden/Modules/notLoadedSavedModuleContents
    //mden 
    //mden Notification of a failed attempt to execute
    //mden `loadedSavedModuleContents` method.
    //mden 
    this.notLoadedSavedModuleContents = this.неЗагрузилиСодержимоеСохранённогоМодуля;

    //mdru/Модули/загрузитьСодержимоеСохранённогоМодуля
    //mdru 
    //mdru Загружает содержимое сохранённого модуля по указателю.
    //mdru 
    this.загрузитьСодержимоеСохранённогоМодуля = function(указатель)
    {
        var модуль = this.модульПоУказателю(указатель);
        if (!модуль)
        {
            this.ошибка = "ОШИБКА Сохранённого модуля с таким указателем не существует | ERROR There is no saved module under such pointer"; 
            this.error = this.ошибка;
            this.неЗагрузилиСодержимоеСохранённогоМодуля.уведомить();
            return;
        }

        var тут = this;
        localforage.getItem(
            указатель,
            function(ошибка, содержимое)
            {
                if (содержимое)
                {
                    модуль.содержимое = содержимое;
                    тут.загрузилиСодержимоеСохранённогоМодуля.уведомить();
                }
                else
                {
                    this.ошибка = ошибка;
                    this.error = this.ошибка;
                    тут.неЗагрузилиСодержимоеСохранённогоМодуля.уведомить();
                }
            }
        );
    };

    //mden/Modules/loadSavedModuleContents
    //mden 
    //mden Load saved module contents by pointer.
    //mden
    this.loadSavedModuleContents = this.загрузитьСодержимоеСохранённогоМодуля;

    //mdru/Модули/сохранили
    //mdru 
    //mdru Уведомление об успешном исполнении метода `сохранить`.
    //mdru 
    this.сохранили = new мир.Уведомитель();

    //mden/Modules/saved
    //mden 
    //mden Notification of a successful completion of `save` method.
    //mden 
    this.saved = this.сохранили;

    //mdru/Модули/неСохранили
    //mdru 
    //mdru Уведомление о неудачном исполнении метода `сохранить`.
    //mdru 
    this.неСохранили = new мир.Уведомитель();

    //mden/Modules/notSaved
    //mden 
    //mden Notification of a failed attempt to execute `save` method.
    //mden 
    this.notSaved = this.неСохранили;

    //mdru/Модули/сохранить
    //mdru 
    //mdru Сохраняет модуль в хранилище браузера.
    //mdru 
    this.сохранить = function(указатель)
    {
        var модуль = this.модули[указатель];
        var тут = this;
        // Сначала кэшируем в IndexedDB, а затем в LocalStorage.
        // First cache into IndexedDB, then to LocalStorage.
        localforage.setItem(
            указатель,
            модуль.содержимое,
            function(ошибка, значение) {
                if (ошибка)
                {
                    тут.ошибка = ошибка;
                    тут.error = ошибка;
                    тут.неСохранили.уведомить();
                }
                else
                {
                    var описание = {
                        "имя": модуль.имя,
                        "версия": модуль.версия,
                        "🏠": модуль["🏠"],
                        "структура": модуль.структура,
                    };
                    localStorage.setItem(указатель, JSON.stringify(описание));
                    тут.сохранили.уведомить();
                }
            }
        );
    };

    //mden/Modules/save
    //mden 
    //mden Save module into browser storage.
    //mden
    this.save = this.сохранить;

    this._создать(ключ, мир);

}


// Частные функции класса `Модули`.
// Private functions of `Modules` class.


Модули.prototype._создать = function(ключ, мир)
{
    localforage.config({ name: ключ });
    this.модули = {};
    this._загрузитьОписаниеСохранённыхМодулей();
    this._создатьМирИспользования(мир);
};


// // // //


Модули.prototype._расшифроватьУказатели = function(указатели)
{
    var расшифровка = [];
    for (var номер in указатели)
    {
        var указатель = указатели[номер];
        расшифровка.push(decodeURI(указатель));
    }
    return расшифровка;
};


// // // //


Модули.prototype._загрузитьОписаниеСохранённыхМодулей = function()
{
    var указатели = [];
    for (var номер = 0; номер < localStorage.length; ++номер)
    {
        var ключ = localStorage.key(номер);
        if (ключ.startsWith("http"))
        {
            указатели.push(ключ);
        }
    }

    for (var номер in указатели)
    {
        var указатель = указатели[номер];
        var представление = localStorage.getItem(указатель);
        if (представление)
        {
            var описание = JSON.parse(представление);
            var модуль =
                new Модуль(
                    описание.имя, 
                    описание.версия,
                    описание["🏠"],
                    указатель,
                    указатель + "/..",
                    описание.структура
                );
            this.модули[указатель] = модуль;
        }
    }
};


// // // //


Модули.prototype._создатьМирИспользования = function(мир)
{
    this.исп = new мир.Мир();

    this.исп.мир = мир;
    this.исп.модули = this.модули;
    this.исп.использовали = this.использовали;
    this.исп.неИспользовали = this.неИспользовали;
    this.исп.неСохранили = this.неСохранили;

    var тут = this;
    this.исп.изменилиСообщениеОтладки.подписать(function() {
        тут.отладка = тут.исп.сообщениеОтладки;
        тут.debug = тут.отладка;
        тут.отладили.уведомить();
    });

    this.исп.разобрать(`
использовать
    загрузить следующий модуль
проверить наличие модуля в кэше
    загрузить содержимое модуля из кэша
загрузили содержимое модуля из кэша
    загрузить следующий модуль
не загрузили содержимое модуля из кэша
    загрузить описание модуля из сети
ошибка загрузки
    уведомить об ошибке использования
загрузили описание модуля из сети
    загрузить содержимое модуля из сети
загрузить файлы модуля из сети
    загрузить следующий файл
загрузили файл
    загрузить следующий файл
загрузили файлы
    загрузить следующий модуль
загрузили модули
    исполнить файлы модулей
    исполнить черёды модулей
    сохранить загруженные модули
    уведомить об успешном использовании
ошибка сохранения
    уведомить об ошибке сохранения
    `);
};


// Реакции для миров класса `Модули`.
// Reactions for the worlds of `Modules` class.


function ЗагрузитьСледующийМодуль(мир)
{
    мир.указатель = мир.указатели.shift()

    мир.отладить(`ЗагрузитьСледующийМодуль указатель: '${мир.указатель}'`);

    if (мир.указатель)
    {
        мир.уведомить("проверить наличие модуля в кэше")
    }
    else
    {
        мир.уведомить("загрузили модули");
    }
}


// // // //


function ЗагрузитьСодержимоеМодуляИзКэша(мир)
{
    мир.отладить(`ЗагрузитьСодержимоеМодуляИзКэша. указатель: '${мир.указатель}'`);

    var модуль = мир.модули[мир.указатель];
    if (модуль)
    {
        localforage.getItem(
            мир.указатель,
            function(ошибка, содержимое)
            {
                if (содержимое)
                {
                    модуль.содержимое = содержимое;
                    мир.уведомить("загрузили содержимое модуля из кэша");
                }
                else
                {
                    мир.уведомить("не загрузили содержимое модуля из кэша");
                }
            }
        );
    }
    else
    {
        мир.уведомить("не загрузили содержимое модуля из кэша");
    }
}


// // // //


function ЗагрузитьОписаниеМодуляИзСети(мир)
{
    мир.мир.взять(
        мир.указатель,
        function(ответ)
        {
            var описание = разобрать0000(ответ);
            var модуль =
                new Модуль(
                    описание.имя,
                    описание.версия,
                    описание["🏠"],
                    мир.указатель,
                    мир.указатель + "/..",
                    описание.структура
                );
            мир.модули[мир.указатель] = модуль;
            мир.модуль = модуль;
            мир.уведомить("загрузили описание модуля из сети");
        },
        function(ошибка)
        {
            мир.тут.ошибка = ошибка;
            мир.тут.error = ошибка;
            мир.уведомить("ошибка загрузки");
        }
    );
}


// // // //


function УведомитьОбОшибкеИспользования(мир)
{
    мир.неИспользовали.уведомить();
}


// // // //


function ЗагрузитьСодержимоеМодуляИзСети(мир)
{
    мир.файлы = [];
    for (var имя in мир.модуль.структура)
    {
        var свойства = мир.модуль.структура[имя];
        var адрес = мир.модуль.http + имя;
        var файл = {
            "имя": имя,
            "адрес": адрес,
            "двоичный": свойства.двоичный,
        };
        мир.файлы.push(файл);
    };
    мир.уведомить("загрузить файлы модуля из сети");
}


// // // //


function ЗагрузитьСледующийФайл(мир)
{
    мир.файл = мир.файлы.shift()

    мир.отладить(`ЗагрузитьСледующийФайл. файл: '${JSON.stringify(мир.файл)}'`);

    if (мир.файл)
    {
        мир.мир.взять(
            мир.файл.адрес,
            function(содержимое)
            {
                if (!мир.модуль.содержимое)
                {
                    мир.модуль.содержимое = {};
                }

                мир.модуль.содержимое[мир.файл.имя] = содержимое;
                мир.уведомить("загрузили файл");
            },
            function(ошибка)
            {
                мир.тут.ошибка = ошибка;
                мир.тут.error = ошибка;
                мир.уведомить("ошибка загрузки");
            },
            мир.файл.двоичный
        );
    }
    else
    {
        мир.уведомить("загрузили файлы");
    }
}


// // // //


function СохранитьЗагруженныеМодули(мир)
{
    for (var номер in мир.используемыеУказатели)
    {
        const указатель = мир.используемыеУказатели[номер];
        мир.отладить(`СохранитьЗагруженныеМодули. указатель: '${указатель}'`);
        const модуль = мир.модули[указатель];
        // Сначала кэшируем в IndexedDB, а затем в LocalStorage.
        // First cache into IndexedDB, then to LocalStorage.
        localforage.setItem(
            указатель,
            модуль.содержимое,
            function(ошибка, значение) {
                if (ошибка)
                {
                    мир.тут.ошибка = ошибка;
                    мир.тут.error = ошибка;
                    мир.уведомить("ошибка сохранения");
                }
                else
                {
                    var описание = {
                        "имя": модуль.имя,
                        "версия": модуль.версия,
                        "🏠": модуль["🏠"],
                        "структура": модуль.структура,
                    };
                    localStorage.setItem(указатель, JSON.stringify(описание));
                }
            }
        );
    }
}


// // // //


function ИсполнитьФайлыМодулей(мир)
{
    for (var номер in мир.используемыеУказатели)
    {
        var указатель = мир.используемыеУказатели[номер];
        мир.отладить(`ИсполнитьФайлыМодулей. указатель: '${указатель}'`);
        var модуль = мир.модули[указатель];
        модуль.исполнить();
    }
}


// // // //


function ИсполнитьЧерёдыМодулей(мир)
{
    for (var номер in мир.используемыеУказатели)
    {
        var указатель = мир.используемыеУказатели[номер];
        мир.отладить(`ИсполнитьЧерёдыМодулей. указатель: '${указатель}'`);
        var модуль = мир.модули[указатель];
        модуль.исполнитьЧерёды(мир.мир);
    }
}


// // // //


function УведомитьОбУспешномИспользовании(мир)
{
    мир.использовали.уведомить();
}


// // // //


function УведомитьОбОшибкеСохранения(мир)
{
    мир.неСохранили.уведомить();
}


