/*
 * 
 * Уведомитель, реализация шаблона "издатель-подписчик".
 * Reporter, an implementation of "publisher-subscriber" pattern.
 *
 */
function Уведомитель()
{
    function Подписка(id, отклик, уведомитель)
    {
        this.id = id;
        this.отклик = отклик;
        this.уведомитель = уведомитель;
    };

    this.уведомить = function()
    {
        // Попутно собираем подписке без отклика.
        var безотклика = [];

        for (var номер in this.подписки)
        {
            var подписка = this.подписки[номер];
            if (подписка.отклик)
            {
                подписка.отклик();
            }
            else
            {
                безотклика.push(подписка);
            }
        }

        // И удаляем их при наличии.
        if (безотклика.length)
        {
            this._удалитьПодпискиБезОтклика(безотклика);
        }
    };

    this.подписать = function(отклик)
    {
        var id = gitjs.uuid();
        var подписка = new Подписка(id, отклик, this);
        this.подписки.push(подписка);
        return подписка;
    };

    this.отписать = function(подписка)
    {
        подписка.отклик = null;
    };

    this.подписатьРаз = function(отклик)
    {
        var тут = this;
        var подписка = this.подписать(function() {
            тут.отписать(подписка);
            отклик();
        });
    };

    this.подписатьМного = function(функции)
    {
        for (var номер = 0; номер < функции.length; ++номер)
        {
            var функция = функции[номер];
            this.подписать(функция);
        }
    }

    this._удалитьПодпискиБезОтклика = function(удалить)
    {
        var подписка = удалить.shift()
        while (подписка)
        {
            var индекс = this.подписки.indexOf(подписка);
            if (индекс !== -1)
            {
                this.подписки.splice(индекс, 1);
            }
            var подписка = удалить.shift()
        }
    };

    // Конструктор.
    // Constructor.
    this.подписки = [];

    // English.
    this.report = this.уведомить;
	this.subscribe = this.подписать;
    this.unsubscribe = this.отписать;
	this.subscribeOnce = this.подписатьРаз;
	this.subscribeMany = this.подписатьМного;
}
