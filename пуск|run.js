// Точка входа.
// Entry point.
function запуститьGitJS()
{
    gitjs.мир = new Мир();
    var мир = gitjs.мир;
    мир.взять = взять;
    мир.разобрать0000 = разобрать0000;
    мир.собрать0000 = собрать0000;
    мир.сделатьПоследовательно = сделатьПоследовательно;
    мир.сделатьПараллельно = сделатьПараллельно;
    мир.Мир = Мир;
    мир.Уведомитель = Уведомитель;
    мир.модули = new Модули("gitjs", мир);

    мир.uuid = gitjs.uuid;

    gitjs.world = мир;
    var world = мир;
    world.get = взять;
    world.parse0000 = разобрать0000;
    world.assemble0000 = собрать0000;
    world.doSequentially = сделатьПоследовательно;
    world.doInParallel = сделатьПараллельно;
    world.World = Мир;
    world.Reporter = Уведомитель;
    world.modules = мир.модули;

    var указатель = "https://git.opengamestudio.org/BCE/BCE/raw/branch/master/0000";
    
    var запрос = window.location.search.substring(1);
    if (запрос.startsWith("http"))
    {
        var конец = запрос.indexOf("/0000");
        if (конец != -1)
        {
            указатель = запрос.substring(0, конец + 5);
        }
    }

    указатель = decodeURI(указатель);

    // Выводить в консоль отладочные сообщения Модулей.
    мир.модули.отладили.подписать(function() {
        console.debug(мир.модули.отладка);
    });

    мир.модули.использовали.подписатьРаз(function() {
        console.debug("Использовали. Запускаем...");
        мир.уведомить("пуск");
    });
    мир.модули.неИспользовали.подписатьРаз(function() {
        var сообщение = `ОШИБКА Не удалось использовать модули | ERROR Could not use modules '${мир.модули.ошибка}'`;
        console.error(сообщение);
        document.body.innerHTML += `<p>${сообщение}</p>`;
    });
    мир.модули.неСохранили.подписатьРаз(function() {
        var сообщение = `ОШИБКА Не удалось сохранить модули в кэше | ERROR Could not cache modules '${мир.модули.ошибка}'`;
        console.error(сообщение);
    });

    var сообщение = "Загрузка модуля ⚬ 模块加载 ⚬ Loading module";
    console.debug(сообщение);
    console.debug(указатель);

    мир.модули.использовать([указатель]);
}
