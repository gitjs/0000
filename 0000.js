gitjs["👵"] = "1.2.3";
/*!
    localForage -- Offline Storage, Improved
    Version 1.7.3
    https://localforage.github.io/localForage
    (c) 2013-2017 Mozilla, Apache License 2.0
*/
!function(a){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=a();else if("function"==typeof define&&define.amd)define([],a);else{var b;b="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,b.localforage=a()}}(function(){return function a(b,c,d){function e(g,h){if(!c[g]){if(!b[g]){var i="function"==typeof require&&require;if(!h&&i)return i(g,!0);if(f)return f(g,!0);var j=new Error("Cannot find module '"+g+"'");throw j.code="MODULE_NOT_FOUND",j}var k=c[g]={exports:{}};b[g][0].call(k.exports,function(a){var c=b[g][1][a];return e(c||a)},k,k.exports,a,b,c,d)}return c[g].exports}for(var f="function"==typeof require&&require,g=0;g<d.length;g++)e(d[g]);return e}({1:[function(a,b,c){"use strict";function d(a,b){if(!(a instanceof b))throw new TypeError("Cannot call a class as a function")}function e(){try{if("undefined"!=typeof indexedDB)return indexedDB;if("undefined"!=typeof webkitIndexedDB)return webkitIndexedDB;if("undefined"!=typeof mozIndexedDB)return mozIndexedDB;if("undefined"!=typeof OIndexedDB)return OIndexedDB;if("undefined"!=typeof msIndexedDB)return msIndexedDB}catch(a){return}}function f(){try{if(!ua)return!1;var a="undefined"!=typeof openDatabase&&/(Safari|iPhone|iPad|iPod)/.test(navigator.userAgent)&&!/Chrome/.test(navigator.userAgent)&&!/BlackBerry/.test(navigator.platform),b="function"==typeof fetch&&-1!==fetch.toString().indexOf("[native code");return(!a||b)&&"undefined"!=typeof indexedDB&&"undefined"!=typeof IDBKeyRange}catch(a){return!1}}function g(a,b){a=a||[],b=b||{};try{return new Blob(a,b)}catch(f){if("TypeError"!==f.name)throw f;for(var c="undefined"!=typeof BlobBuilder?BlobBuilder:"undefined"!=typeof MSBlobBuilder?MSBlobBuilder:"undefined"!=typeof MozBlobBuilder?MozBlobBuilder:WebKitBlobBuilder,d=new c,e=0;e<a.length;e+=1)d.append(a[e]);return d.getBlob(b.type)}}function h(a,b){b&&a.then(function(a){b(null,a)},function(a){b(a)})}function i(a,b,c){"function"==typeof b&&a.then(b),"function"==typeof c&&a.catch(c)}function j(a){return"string"!=typeof a&&(console.warn(a+" used as a key, but it is not a string."),a=String(a)),a}function k(){if(arguments.length&&"function"==typeof arguments[arguments.length-1])return arguments[arguments.length-1]}function l(a){for(var b=a.length,c=new ArrayBuffer(b),d=new Uint8Array(c),e=0;e<b;e++)d[e]=a.charCodeAt(e);return c}function m(a){return new va(function(b){var c=a.transaction(wa,Ba),d=g([""]);c.objectStore(wa).put(d,"key"),c.onabort=function(a){a.preventDefault(),a.stopPropagation(),b(!1)},c.oncomplete=function(){var a=navigator.userAgent.match(/Chrome\/(\d+)/),c=navigator.userAgent.match(/Edge\//);b(c||!a||parseInt(a[1],10)>=43)}}).catch(function(){return!1})}function n(a){return"boolean"==typeof xa?va.resolve(xa):m(a).then(function(a){return xa=a})}function o(a){var b=ya[a.name],c={};c.promise=new va(function(a,b){c.resolve=a,c.reject=b}),b.deferredOperations.push(c),b.dbReady?b.dbReady=b.dbReady.then(function(){return c.promise}):b.dbReady=c.promise}function p(a){var b=ya[a.name],c=b.deferredOperations.pop();if(c)return c.resolve(),c.promise}function q(a,b){var c=ya[a.name],d=c.deferredOperations.pop();if(d)return d.reject(b),d.promise}function r(a,b){return new va(function(c,d){if(ya[a.name]=ya[a.name]||B(),a.db){if(!b)return c(a.db);o(a),a.db.close()}var e=[a.name];b&&e.push(a.version);var f=ua.open.apply(ua,e);b&&(f.onupgradeneeded=function(b){var c=f.result;try{c.createObjectStore(a.storeName),b.oldVersion<=1&&c.createObjectStore(wa)}catch(c){if("ConstraintError"!==c.name)throw c;console.warn('The database "'+a.name+'" has been upgraded from version '+b.oldVersion+" to version "+b.newVersion+', but the storage "'+a.storeName+'" already exists.')}}),f.onerror=function(a){a.preventDefault(),d(f.error)},f.onsuccess=function(){c(f.result),p(a)}})}function s(a){return r(a,!1)}function t(a){return r(a,!0)}function u(a,b){if(!a.db)return!0;var c=!a.db.objectStoreNames.contains(a.storeName),d=a.version<a.db.version,e=a.version>a.db.version;if(d&&(a.version!==b&&console.warn('The database "'+a.name+"\" can't be downgraded from version "+a.db.version+" to version "+a.version+"."),a.version=a.db.version),e||c){if(c){var f=a.db.version+1;f>a.version&&(a.version=f)}return!0}return!1}function v(a){return new va(function(b,c){var d=new FileReader;d.onerror=c,d.onloadend=function(c){var d=btoa(c.target.result||"");b({__local_forage_encoded_blob:!0,data:d,type:a.type})},d.readAsBinaryString(a)})}function w(a){return g([l(atob(a.data))],{type:a.type})}function x(a){return a&&a.__local_forage_encoded_blob}function y(a){var b=this,c=b._initReady().then(function(){var a=ya[b._dbInfo.name];if(a&&a.dbReady)return a.dbReady});return i(c,a,a),c}function z(a){o(a);for(var b=ya[a.name],c=b.forages,d=0;d<c.length;d++){var e=c[d];e._dbInfo.db&&(e._dbInfo.db.close(),e._dbInfo.db=null)}return a.db=null,s(a).then(function(b){return a.db=b,u(a)?t(a):b}).then(function(d){a.db=b.db=d;for(var e=0;e<c.length;e++)c[e]._dbInfo.db=d}).catch(function(b){throw q(a,b),b})}function A(a,b,c,d){void 0===d&&(d=1);try{var e=a.db.transaction(a.storeName,b);c(null,e)}catch(e){if(d>0&&(!a.db||"InvalidStateError"===e.name||"NotFoundError"===e.name))return va.resolve().then(function(){if(!a.db||"NotFoundError"===e.name&&!a.db.objectStoreNames.contains(a.storeName)&&a.version<=a.db.version)return a.db&&(a.version=a.db.version+1),t(a)}).then(function(){return z(a).then(function(){A(a,b,c,d-1)})}).catch(c);c(e)}}function B(){return{forages:[],db:null,dbReady:null,deferredOperations:[]}}function C(a){function b(){return va.resolve()}var c=this,d={db:null};if(a)for(var e in a)d[e]=a[e];var f=ya[d.name];f||(f=B(),ya[d.name]=f),f.forages.push(c),c._initReady||(c._initReady=c.ready,c.ready=y);for(var g=[],h=0;h<f.forages.length;h++){var i=f.forages[h];i!==c&&g.push(i._initReady().catch(b))}var j=f.forages.slice(0);return va.all(g).then(function(){return d.db=f.db,s(d)}).then(function(a){return d.db=a,u(d,c._defaultConfig.version)?t(d):a}).then(function(a){d.db=f.db=a,c._dbInfo=d;for(var b=0;b<j.length;b++){var e=j[b];e!==c&&(e._dbInfo.db=d.db,e._dbInfo.version=d.version)}})}function D(a,b){var c=this;a=j(a);var d=new va(function(b,d){c.ready().then(function(){A(c._dbInfo,Aa,function(e,f){if(e)return d(e);try{var g=f.objectStore(c._dbInfo.storeName),h=g.get(a);h.onsuccess=function(){var a=h.result;void 0===a&&(a=null),x(a)&&(a=w(a)),b(a)},h.onerror=function(){d(h.error)}}catch(a){d(a)}})}).catch(d)});return h(d,b),d}function E(a,b){var c=this,d=new va(function(b,d){c.ready().then(function(){A(c._dbInfo,Aa,function(e,f){if(e)return d(e);try{var g=f.objectStore(c._dbInfo.storeName),h=g.openCursor(),i=1;h.onsuccess=function(){var c=h.result;if(c){var d=c.value;x(d)&&(d=w(d));var e=a(d,c.key,i++);void 0!==e?b(e):c.continue()}else b()},h.onerror=function(){d(h.error)}}catch(a){d(a)}})}).catch(d)});return h(d,b),d}function F(a,b,c){var d=this;a=j(a);var e=new va(function(c,e){var f;d.ready().then(function(){return f=d._dbInfo,"[object Blob]"===za.call(b)?n(f.db).then(function(a){return a?b:v(b)}):b}).then(function(b){A(d._dbInfo,Ba,function(f,g){if(f)return e(f);try{var h=g.objectStore(d._dbInfo.storeName);null===b&&(b=void 0);var i=h.put(b,a);g.oncomplete=function(){void 0===b&&(b=null),c(b)},g.onabort=g.onerror=function(){var a=i.error?i.error:i.transaction.error;e(a)}}catch(a){e(a)}})}).catch(e)});return h(e,c),e}function G(a,b){var c=this;a=j(a);var d=new va(function(b,d){c.ready().then(function(){A(c._dbInfo,Ba,function(e,f){if(e)return d(e);try{var g=f.objectStore(c._dbInfo.storeName),h=g.delete(a);f.oncomplete=function(){b()},f.onerror=function(){d(h.error)},f.onabort=function(){var a=h.error?h.error:h.transaction.error;d(a)}}catch(a){d(a)}})}).catch(d)});return h(d,b),d}function H(a){var b=this,c=new va(function(a,c){b.ready().then(function(){A(b._dbInfo,Ba,function(d,e){if(d)return c(d);try{var f=e.objectStore(b._dbInfo.storeName),g=f.clear();e.oncomplete=function(){a()},e.onabort=e.onerror=function(){var a=g.error?g.error:g.transaction.error;c(a)}}catch(a){c(a)}})}).catch(c)});return h(c,a),c}function I(a){var b=this,c=new va(function(a,c){b.ready().then(function(){A(b._dbInfo,Aa,function(d,e){if(d)return c(d);try{var f=e.objectStore(b._dbInfo.storeName),g=f.count();g.onsuccess=function(){a(g.result)},g.onerror=function(){c(g.error)}}catch(a){c(a)}})}).catch(c)});return h(c,a),c}function J(a,b){var c=this,d=new va(function(b,d){if(a<0)return void b(null);c.ready().then(function(){A(c._dbInfo,Aa,function(e,f){if(e)return d(e);try{var g=f.objectStore(c._dbInfo.storeName),h=!1,i=g.openCursor();i.onsuccess=function(){var c=i.result;if(!c)return void b(null);0===a?b(c.key):h?b(c.key):(h=!0,c.advance(a))},i.onerror=function(){d(i.error)}}catch(a){d(a)}})}).catch(d)});return h(d,b),d}function K(a){var b=this,c=new va(function(a,c){b.ready().then(function(){A(b._dbInfo,Aa,function(d,e){if(d)return c(d);try{var f=e.objectStore(b._dbInfo.storeName),g=f.openCursor(),h=[];g.onsuccess=function(){var b=g.result;if(!b)return void a(h);h.push(b.key),b.continue()},g.onerror=function(){c(g.error)}}catch(a){c(a)}})}).catch(c)});return h(c,a),c}function L(a,b){b=k.apply(this,arguments);var c=this.config();a="function"!=typeof a&&a||{},a.name||(a.name=a.name||c.name,a.storeName=a.storeName||c.storeName);var d,e=this;if(a.name){var f=a.name===c.name&&e._dbInfo.db,g=f?va.resolve(e._dbInfo.db):s(a).then(function(b){var c=ya[a.name],d=c.forages;c.db=b;for(var e=0;e<d.length;e++)d[e]._dbInfo.db=b;return b});d=a.storeName?g.then(function(b){if(b.objectStoreNames.contains(a.storeName)){var c=b.version+1;o(a);var d=ya[a.name],e=d.forages;b.close();for(var f=0;f<e.length;f++){var g=e[f];g._dbInfo.db=null,g._dbInfo.version=c}return new va(function(b,d){var e=ua.open(a.name,c);e.onerror=function(a){e.result.close(),d(a)},e.onupgradeneeded=function(){e.result.deleteObjectStore(a.storeName)},e.onsuccess=function(){var a=e.result;a.close(),b(a)}}).then(function(a){d.db=a;for(var b=0;b<e.length;b++){var c=e[b];c._dbInfo.db=a,p(c._dbInfo)}}).catch(function(b){throw(q(a,b)||va.resolve()).catch(function(){}),b})}}):g.then(function(b){o(a);var c=ya[a.name],d=c.forages;b.close();for(var e=0;e<d.length;e++){d[e]._dbInfo.db=null}return new va(function(b,c){var d=ua.deleteDatabase(a.name);d.onerror=d.onblocked=function(a){var b=d.result;b&&b.close(),c(a)},d.onsuccess=function(){var a=d.result;a&&a.close(),b(a)}}).then(function(a){c.db=a;for(var b=0;b<d.length;b++)p(d[b]._dbInfo)}).catch(function(b){throw(q(a,b)||va.resolve()).catch(function(){}),b})})}else d=va.reject("Invalid arguments");return h(d,b),d}function M(){return"function"==typeof openDatabase}function N(a){var b,c,d,e,f,g=.75*a.length,h=a.length,i=0;"="===a[a.length-1]&&(g--,"="===a[a.length-2]&&g--);var j=new ArrayBuffer(g),k=new Uint8Array(j);for(b=0;b<h;b+=4)c=Da.indexOf(a[b]),d=Da.indexOf(a[b+1]),e=Da.indexOf(a[b+2]),f=Da.indexOf(a[b+3]),k[i++]=c<<2|d>>4,k[i++]=(15&d)<<4|e>>2,k[i++]=(3&e)<<6|63&f;return j}function O(a){var b,c=new Uint8Array(a),d="";for(b=0;b<c.length;b+=3)d+=Da[c[b]>>2],d+=Da[(3&c[b])<<4|c[b+1]>>4],d+=Da[(15&c[b+1])<<2|c[b+2]>>6],d+=Da[63&c[b+2]];return c.length%3==2?d=d.substring(0,d.length-1)+"=":c.length%3==1&&(d=d.substring(0,d.length-2)+"=="),d}function P(a,b){var c="";if(a&&(c=Ua.call(a)),a&&("[object ArrayBuffer]"===c||a.buffer&&"[object ArrayBuffer]"===Ua.call(a.buffer))){var d,e=Ga;a instanceof ArrayBuffer?(d=a,e+=Ia):(d=a.buffer,"[object Int8Array]"===c?e+=Ka:"[object Uint8Array]"===c?e+=La:"[object Uint8ClampedArray]"===c?e+=Ma:"[object Int16Array]"===c?e+=Na:"[object Uint16Array]"===c?e+=Pa:"[object Int32Array]"===c?e+=Oa:"[object Uint32Array]"===c?e+=Qa:"[object Float32Array]"===c?e+=Ra:"[object Float64Array]"===c?e+=Sa:b(new Error("Failed to get type for BinaryArray"))),b(e+O(d))}else if("[object Blob]"===c){var f=new FileReader;f.onload=function(){var c=Ea+a.type+"~"+O(this.result);b(Ga+Ja+c)},f.readAsArrayBuffer(a)}else try{b(JSON.stringify(a))}catch(c){console.error("Couldn't convert value into a JSON string: ",a),b(null,c)}}function Q(a){if(a.substring(0,Ha)!==Ga)return JSON.parse(a);var b,c=a.substring(Ta),d=a.substring(Ha,Ta);if(d===Ja&&Fa.test(c)){var e=c.match(Fa);b=e[1],c=c.substring(e[0].length)}var f=N(c);switch(d){case Ia:return f;case Ja:return g([f],{type:b});case Ka:return new Int8Array(f);case La:return new Uint8Array(f);case Ma:return new Uint8ClampedArray(f);case Na:return new Int16Array(f);case Pa:return new Uint16Array(f);case Oa:return new Int32Array(f);case Qa:return new Uint32Array(f);case Ra:return new Float32Array(f);case Sa:return new Float64Array(f);default:throw new Error("Unkown type: "+d)}}function R(a,b,c,d){a.executeSql("CREATE TABLE IF NOT EXISTS "+b.storeName+" (id INTEGER PRIMARY KEY, key unique, value)",[],c,d)}function S(a){var b=this,c={db:null};if(a)for(var d in a)c[d]="string"!=typeof a[d]?a[d].toString():a[d];var e=new va(function(a,d){try{c.db=openDatabase(c.name,String(c.version),c.description,c.size)}catch(a){return d(a)}c.db.transaction(function(e){R(e,c,function(){b._dbInfo=c,a()},function(a,b){d(b)})},d)});return c.serializer=Va,e}function T(a,b,c,d,e,f){a.executeSql(c,d,e,function(a,g){g.code===g.SYNTAX_ERR?a.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name = ?",[b.storeName],function(a,h){h.rows.length?f(a,g):R(a,b,function(){a.executeSql(c,d,e,f)},f)},f):f(a,g)},f)}function U(a,b){var c=this;a=j(a);var d=new va(function(b,d){c.ready().then(function(){var e=c._dbInfo;e.db.transaction(function(c){T(c,e,"SELECT * FROM "+e.storeName+" WHERE key = ? LIMIT 1",[a],function(a,c){var d=c.rows.length?c.rows.item(0).value:null;d&&(d=e.serializer.deserialize(d)),b(d)},function(a,b){d(b)})})}).catch(d)});return h(d,b),d}function V(a,b){var c=this,d=new va(function(b,d){c.ready().then(function(){var e=c._dbInfo;e.db.transaction(function(c){T(c,e,"SELECT * FROM "+e.storeName,[],function(c,d){for(var f=d.rows,g=f.length,h=0;h<g;h++){var i=f.item(h),j=i.value;if(j&&(j=e.serializer.deserialize(j)),void 0!==(j=a(j,i.key,h+1)))return void b(j)}b()},function(a,b){d(b)})})}).catch(d)});return h(d,b),d}function W(a,b,c,d){var e=this;a=j(a);var f=new va(function(f,g){e.ready().then(function(){void 0===b&&(b=null);var h=b,i=e._dbInfo;i.serializer.serialize(b,function(b,j){j?g(j):i.db.transaction(function(c){T(c,i,"INSERT OR REPLACE INTO "+i.storeName+" (key, value) VALUES (?, ?)",[a,b],function(){f(h)},function(a,b){g(b)})},function(b){if(b.code===b.QUOTA_ERR){if(d>0)return void f(W.apply(e,[a,h,c,d-1]));g(b)}})})}).catch(g)});return h(f,c),f}function X(a,b,c){return W.apply(this,[a,b,c,1])}function Y(a,b){var c=this;a=j(a);var d=new va(function(b,d){c.ready().then(function(){var e=c._dbInfo;e.db.transaction(function(c){T(c,e,"DELETE FROM "+e.storeName+" WHERE key = ?",[a],function(){b()},function(a,b){d(b)})})}).catch(d)});return h(d,b),d}function Z(a){var b=this,c=new va(function(a,c){b.ready().then(function(){var d=b._dbInfo;d.db.transaction(function(b){T(b,d,"DELETE FROM "+d.storeName,[],function(){a()},function(a,b){c(b)})})}).catch(c)});return h(c,a),c}function $(a){var b=this,c=new va(function(a,c){b.ready().then(function(){var d=b._dbInfo;d.db.transaction(function(b){T(b,d,"SELECT COUNT(key) as c FROM "+d.storeName,[],function(b,c){var d=c.rows.item(0).c;a(d)},function(a,b){c(b)})})}).catch(c)});return h(c,a),c}function _(a,b){var c=this,d=new va(function(b,d){c.ready().then(function(){var e=c._dbInfo;e.db.transaction(function(c){T(c,e,"SELECT key FROM "+e.storeName+" WHERE id = ? LIMIT 1",[a+1],function(a,c){var d=c.rows.length?c.rows.item(0).key:null;b(d)},function(a,b){d(b)})})}).catch(d)});return h(d,b),d}function aa(a){var b=this,c=new va(function(a,c){b.ready().then(function(){var d=b._dbInfo;d.db.transaction(function(b){T(b,d,"SELECT key FROM "+d.storeName,[],function(b,c){for(var d=[],e=0;e<c.rows.length;e++)d.push(c.rows.item(e).key);a(d)},function(a,b){c(b)})})}).catch(c)});return h(c,a),c}function ba(a){return new va(function(b,c){a.transaction(function(d){d.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name <> '__WebKitDatabaseInfoTable__'",[],function(c,d){for(var e=[],f=0;f<d.rows.length;f++)e.push(d.rows.item(f).name);b({db:a,storeNames:e})},function(a,b){c(b)})},function(a){c(a)})})}function ca(a,b){b=k.apply(this,arguments);var c=this.config();a="function"!=typeof a&&a||{},a.name||(a.name=a.name||c.name,a.storeName=a.storeName||c.storeName);var d,e=this;return d=a.name?new va(function(b){var d;d=a.name===c.name?e._dbInfo.db:openDatabase(a.name,"","",0),b(a.storeName?{db:d,storeNames:[a.storeName]}:ba(d))}).then(function(a){return new va(function(b,c){a.db.transaction(function(d){function e(a){return new va(function(b,c){d.executeSql("DROP TABLE IF EXISTS "+a,[],function(){b()},function(a,b){c(b)})})}for(var f=[],g=0,h=a.storeNames.length;g<h;g++)f.push(e(a.storeNames[g]));va.all(f).then(function(){b()}).catch(function(a){c(a)})},function(a){c(a)})})}):va.reject("Invalid arguments"),h(d,b),d}function da(){try{return"undefined"!=typeof localStorage&&"setItem"in localStorage&&!!localStorage.setItem}catch(a){return!1}}function ea(a,b){var c=a.name+"/";return a.storeName!==b.storeName&&(c+=a.storeName+"/"),c}function fa(){var a="_localforage_support_test";try{return localStorage.setItem(a,!0),localStorage.removeItem(a),!1}catch(a){return!0}}function ga(){return!fa()||localStorage.length>0}function ha(a){var b=this,c={};if(a)for(var d in a)c[d]=a[d];return c.keyPrefix=ea(a,b._defaultConfig),ga()?(b._dbInfo=c,c.serializer=Va,va.resolve()):va.reject()}function ia(a){var b=this,c=b.ready().then(function(){for(var a=b._dbInfo.keyPrefix,c=localStorage.length-1;c>=0;c--){var d=localStorage.key(c);0===d.indexOf(a)&&localStorage.removeItem(d)}});return h(c,a),c}function ja(a,b){var c=this;a=j(a);var d=c.ready().then(function(){var b=c._dbInfo,d=localStorage.getItem(b.keyPrefix+a);return d&&(d=b.serializer.deserialize(d)),d});return h(d,b),d}function ka(a,b){var c=this,d=c.ready().then(function(){for(var b=c._dbInfo,d=b.keyPrefix,e=d.length,f=localStorage.length,g=1,h=0;h<f;h++){var i=localStorage.key(h);if(0===i.indexOf(d)){var j=localStorage.getItem(i);if(j&&(j=b.serializer.deserialize(j)),void 0!==(j=a(j,i.substring(e),g++)))return j}}});return h(d,b),d}function la(a,b){var c=this,d=c.ready().then(function(){var b,d=c._dbInfo;try{b=localStorage.key(a)}catch(a){b=null}return b&&(b=b.substring(d.keyPrefix.length)),b});return h(d,b),d}function ma(a){var b=this,c=b.ready().then(function(){for(var a=b._dbInfo,c=localStorage.length,d=[],e=0;e<c;e++){var f=localStorage.key(e);0===f.indexOf(a.keyPrefix)&&d.push(f.substring(a.keyPrefix.length))}return d});return h(c,a),c}function na(a){var b=this,c=b.keys().then(function(a){return a.length});return h(c,a),c}function oa(a,b){var c=this;a=j(a);var d=c.ready().then(function(){var b=c._dbInfo;localStorage.removeItem(b.keyPrefix+a)});return h(d,b),d}function pa(a,b,c){var d=this;a=j(a);var e=d.ready().then(function(){void 0===b&&(b=null);var c=b;return new va(function(e,f){var g=d._dbInfo;g.serializer.serialize(b,function(b,d){if(d)f(d);else try{localStorage.setItem(g.keyPrefix+a,b),e(c)}catch(a){"QuotaExceededError"!==a.name&&"NS_ERROR_DOM_QUOTA_REACHED"!==a.name||f(a),f(a)}})})});return h(e,c),e}function qa(a,b){if(b=k.apply(this,arguments),a="function"!=typeof a&&a||{},!a.name){var c=this.config();a.name=a.name||c.name,a.storeName=a.storeName||c.storeName}var d,e=this;return d=a.name?new va(function(b){b(a.storeName?ea(a,e._defaultConfig):a.name+"/")}).then(function(a){for(var b=localStorage.length-1;b>=0;b--){var c=localStorage.key(b);0===c.indexOf(a)&&localStorage.removeItem(c)}}):va.reject("Invalid arguments"),h(d,b),d}function ra(a,b){a[b]=function(){var c=arguments;return a.ready().then(function(){return a[b].apply(a,c)})}}function sa(){for(var a=1;a<arguments.length;a++){var b=arguments[a];if(b)for(var c in b)b.hasOwnProperty(c)&&($a(b[c])?arguments[0][c]=b[c].slice():arguments[0][c]=b[c])}return arguments[0]}var ta="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(a){return typeof a}:function(a){return a&&"function"==typeof Symbol&&a.constructor===Symbol&&a!==Symbol.prototype?"symbol":typeof a},ua=e();"undefined"==typeof Promise&&a("lie/polyfill");var va=Promise,wa="local-forage-detect-blob-support",xa=void 0,ya={},za=Object.prototype.toString,Aa="readonly",Ba="readwrite",Ca={_driver:"asyncStorage",_initStorage:C,_support:f(),iterate:E,getItem:D,setItem:F,removeItem:G,clear:H,length:I,key:J,keys:K,dropInstance:L},Da="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",Ea="~~local_forage_type~",Fa=/^~~local_forage_type~([^~]+)~/,Ga="__lfsc__:",Ha=Ga.length,Ia="arbf",Ja="blob",Ka="si08",La="ui08",Ma="uic8",Na="si16",Oa="si32",Pa="ur16",Qa="ui32",Ra="fl32",Sa="fl64",Ta=Ha+Ia.length,Ua=Object.prototype.toString,Va={serialize:P,deserialize:Q,stringToBuffer:N,bufferToString:O},Wa={_driver:"webSQLStorage",_initStorage:S,_support:M(),iterate:V,getItem:U,setItem:X,removeItem:Y,clear:Z,length:$,key:_,keys:aa,dropInstance:ca},Xa={_driver:"localStorageWrapper",_initStorage:ha,_support:da(),iterate:ka,getItem:ja,setItem:pa,removeItem:oa,clear:ia,length:na,key:la,keys:ma,dropInstance:qa},Ya=function(a,b){return a===b||"number"==typeof a&&"number"==typeof b&&isNaN(a)&&isNaN(b)},Za=function(a,b){for(var c=a.length,d=0;d<c;){if(Ya(a[d],b))return!0;d++}return!1},$a=Array.isArray||function(a){return"[object Array]"===Object.prototype.toString.call(a)},_a={},ab={},bb={INDEXEDDB:Ca,WEBSQL:Wa,LOCALSTORAGE:Xa},cb=[bb.INDEXEDDB._driver,bb.WEBSQL._driver,bb.LOCALSTORAGE._driver],db=["dropInstance"],eb=["clear","getItem","iterate","key","keys","length","removeItem","setItem"].concat(db),fb={description:"",driver:cb.slice(),name:"localforage",size:4980736,storeName:"keyvaluepairs",version:1},gb=function(){function a(b){d(this,a);for(var c in bb)if(bb.hasOwnProperty(c)){var e=bb[c],f=e._driver;this[c]=f,_a[f]||this.defineDriver(e)}this._defaultConfig=sa({},fb),this._config=sa({},this._defaultConfig,b),this._driverSet=null,this._initDriver=null,this._ready=!1,this._dbInfo=null,this._wrapLibraryMethodsWithReady(),this.setDriver(this._config.driver).catch(function(){})}return a.prototype.config=function(a){if("object"===(void 0===a?"undefined":ta(a))){if(this._ready)return new Error("Can't call config() after localforage has been used.");for(var b in a){if("storeName"===b&&(a[b]=a[b].replace(/\W/g,"_")),"version"===b&&"number"!=typeof a[b])return new Error("Database version must be a number.");this._config[b]=a[b]}return!("driver"in a&&a.driver)||this.setDriver(this._config.driver)}return"string"==typeof a?this._config[a]:this._config},a.prototype.defineDriver=function(a,b,c){var d=new va(function(b,c){try{var d=a._driver,e=new Error("Custom driver not compliant; see https://mozilla.github.io/localForage/#definedriver");if(!a._driver)return void c(e);for(var f=eb.concat("_initStorage"),g=0,i=f.length;g<i;g++){var j=f[g];if((!Za(db,j)||a[j])&&"function"!=typeof a[j])return void c(e)}(function(){for(var b=function(a){return function(){var b=new Error("Method "+a+" is not implemented by the current driver"),c=va.reject(b);return h(c,arguments[arguments.length-1]),c}},c=0,d=db.length;c<d;c++){var e=db[c];a[e]||(a[e]=b(e))}})();var k=function(c){_a[d]&&console.info("Redefining LocalForage driver: "+d),_a[d]=a,ab[d]=c,b()};"_support"in a?a._support&&"function"==typeof a._support?a._support().then(k,c):k(!!a._support):k(!0)}catch(a){c(a)}});return i(d,b,c),d},a.prototype.driver=function(){return this._driver||null},a.prototype.getDriver=function(a,b,c){var d=_a[a]?va.resolve(_a[a]):va.reject(new Error("Driver not found."));return i(d,b,c),d},a.prototype.getSerializer=function(a){var b=va.resolve(Va);return i(b,a),b},a.prototype.ready=function(a){var b=this,c=b._driverSet.then(function(){return null===b._ready&&(b._ready=b._initDriver()),b._ready});return i(c,a,a),c},a.prototype.setDriver=function(a,b,c){function d(){g._config.driver=g.driver()}function e(a){return g._extend(a),d(),g._ready=g._initStorage(g._config),g._ready}function f(a){return function(){function b(){for(;c<a.length;){var f=a[c];return c++,g._dbInfo=null,g._ready=null,g.getDriver(f).then(e).catch(b)}d();var h=new Error("No available storage method found.");return g._driverSet=va.reject(h),g._driverSet}var c=0;return b()}}var g=this;$a(a)||(a=[a]);var h=this._getSupportedDrivers(a),j=null!==this._driverSet?this._driverSet.catch(function(){return va.resolve()}):va.resolve();return this._driverSet=j.then(function(){var a=h[0];return g._dbInfo=null,g._ready=null,g.getDriver(a).then(function(a){g._driver=a._driver,d(),g._wrapLibraryMethodsWithReady(),g._initDriver=f(h)})}).catch(function(){d();var a=new Error("No available storage method found.");return g._driverSet=va.reject(a),g._driverSet}),i(this._driverSet,b,c),this._driverSet},a.prototype.supports=function(a){return!!ab[a]},a.prototype._extend=function(a){sa(this,a)},a.prototype._getSupportedDrivers=function(a){for(var b=[],c=0,d=a.length;c<d;c++){var e=a[c];this.supports(e)&&b.push(e)}return b},a.prototype._wrapLibraryMethodsWithReady=function(){for(var a=0,b=eb.length;a<b;a++)ra(this,eb[a])},a.prototype.createInstance=function(b){return new a(b)},a}(),hb=new gb;b.exports=hb},{undefined:void 0}]},{},[1])(1)});
/*
 *
 * Получение данных по HTTP(S).
 * Getting data over HTTP(S).
 *
 */
function взять(адрес, откликУспех, откликПровал = null, двоичные = null)
{
    var запрос = new XMLHttpRequest();
    if (двоичные)
    {
        запрос.responseType = "arraybuffer";
    }
    запрос.onreadystatechange = function()
    {
        if (this.readyState == 4)
        {
            if (this.status == 200)
            {
                откликУспех(двоичные ? this.response : this.responseText);
            }
            else if (откликПровал)
            {
                откликПровал(this.status);
            }
        }
    }
    запрос.open("GET", адрес + "?" + gitjs.uuid());
    запрос.send();
}

//mdru/Функции/разобрать0000

//mden/Functions/parse0000

function разобрать0000(содержимое, файлы = null)
{
    var описание = {
        "имя": null,
        "версия": null,
        "🏠": null,
        "структура": {},
    };

    var строки = содержимое.split("\n");
    var теперьФайлы = false;
    for (номер in строки)
    {
        var строка = строки[номер];

        // Определяем разделитель - пустую строку - описания и списка файлов.
        if (!строка.length)
        {
            теперьФайлы = true;
            continue;
        }

        // Описание.
        if (!теперьФайлы)
        {
            if (!описание.имя)
            {
                описание.имя = строка;
            }
            else if (!описание.версия)
            {
                описание.версия = строка;
            }
            else if (!описание["🏠"])
            {
                описание["🏠"] = строка;
            }
        }
        // Файлы.
        else
        {
            var индекс = строка.indexOf("/");

            // Пропускаем некорректно оформленную строку.
            if (индекс == -1)
            {
                continue;
            }

            var флаги = строка.substring(0, индекс);
            var файл = строка.substring(индекс);
            
            // Заполняем список файлов (при наличии).
            if (файлы)
            {
                файлы.push(файл);
            }

            // Структура.
            описание.структура[файл] = {};
            if (флаги.includes("x"))
            {
                описание.структура[файл]["исполнить"] = true;
            }
            if (флаги.includes("2"))
            {
                описание.структура[файл]["двоичный"] = true;
            }
        }
    }

    return описание;
}

function собрать0000(описание) {
    var вывод = "";
    вывод += `${описание.имя}\n`;
    вывод += `${описание.версия}\n`;
    вывод += `${описание["🏠"]}\n`;
    var файлы = Object.keys(описание.структура).sort();
    for (var номер in файлы)
    {
        var файл = файлы[номер];
        var строкаФлагов = "";
        var флаги = описание.структура[файл];
        for (var флаг in флаги)
        {
            if (флаг == "исполнить")
            {
                строкаФлагов += "x";
            }
            else if (флаг == "двоичный")
            {
                строкаФлагов += "2";
            }
        }
        if (строкаФлагов.length)
        {
            строкаФлагов += " ";
        }
        вывод += `\n${строкаФлагов}${файл}`;
    }

    return вывод;
}

/*
#mdru/Функции/сделатьПоследовательно
#mdru 
#mdru Функция `сделатьПоследовательно(мир, список, функция, откликУспех, откликПровал = null)`
#mdru позволяет исполнить последовательно функцию следующего вида:
#mdru `функция(мир, список, номер, откликУспех, откликПровал)`.
#mdru 
#mdru В частности функция `сделатьПоследовательно()` используется для
#mdru загрузки файлов модулей ГитЖС.
#mdru 

//mden/Functions/doSequentially
//mden 
//mden `doSequentially(world, list, func, callbackSuccess, callbackFailure = null)`
//mden function may execute sequentially a function that looks like this:
//mden `func(world, list, id, callbackSuccess, callbackFailure)`.
//mden 
//mden Specifically, `doSequentially()` function is used to download GitJS
//mden module files.
//mden 
*/

function сделатьПоследовательно(мир, список, функция, откликУспех, откликПровал = null)
{
    var номер = 0;
    функция(мир, список, номер, продолжить, откликПровал);

    function продолжить()
    {
        номер += 1;
        if (номер < список.length)
        {
            функция(мир, список, номер, продолжить, откликПровал);
        }
        else
        {
            откликУспех();
        }
    }
}

function сделатьПараллельно(мир, список, функция, откликУспех, откликПровал = null)
{
    for (const номер in список)
    {
        функция(мир, список, номер, готово, провал);
    }
    
    var всего = 0;

    function готово()
    {
        всего += 1;
        if (всего == список.length)
        {
            откликУспех();
        }
    }
    
    var провалУжеСообщили = false;
    
    function провал()
    {
        if (провалУжеСообщили)
        {
            return;
        }
        провалУжеСообщили = true;
        if (откликПровал)
        {
            откликПровал();
        }
    }
}

/*
 * 
 * Уведомитель, реализация шаблона "издатель-подписчик".
 * Reporter, an implementation of "publisher-subscriber" pattern.
 *
 */
function Уведомитель()
{
    function Подписка(id, отклик, уведомитель)
    {
        this.id = id;
        this.отклик = отклик;
        this.уведомитель = уведомитель;
    };

    this.уведомить = function()
    {
        // Попутно собираем подписке без отклика.
        var безотклика = [];

        for (var номер in this.подписки)
        {
            var подписка = this.подписки[номер];
            if (подписка.отклик)
            {
                подписка.отклик();
            }
            else
            {
                безотклика.push(подписка);
            }
        }

        // И удаляем их при наличии.
        if (безотклика.length)
        {
            this._удалитьПодпискиБезОтклика(безотклика);
        }
    };

    this.подписать = function(отклик)
    {
        var id = gitjs.uuid();
        var подписка = new Подписка(id, отклик, this);
        this.подписки.push(подписка);
        return подписка;
    };

    this.отписать = function(подписка)
    {
        подписка.отклик = null;
    };

    this.подписатьРаз = function(отклик)
    {
        var тут = this;
        var подписка = this.подписать(function() {
            тут.отписать(подписка);
            отклик();
        });
    };

    this.подписатьМного = function(функции)
    {
        for (var номер = 0; номер < функции.length; ++номер)
        {
            var функция = функции[номер];
            this.подписать(функция);
        }
    }

    this._удалитьПодпискиБезОтклика = function(удалить)
    {
        var подписка = удалить.shift()
        while (подписка)
        {
            var индекс = this.подписки.indexOf(подписка);
            if (индекс !== -1)
            {
                this.подписки.splice(индекс, 1);
            }
            var подписка = удалить.shift()
        }
    };

    // Конструктор.
    // Constructor.
    this.подписки = [];

    // English.
    this.report = this.уведомить;
	this.subscribe = this.подписать;
    this.unsubscribe = this.отписать;
	this.subscribeOnce = this.подписатьРаз;
	this.subscribeMany = this.подписатьМного;
}

/*
 *
 * Мир, описание логики в понятных последовательностях (черёдах).
 * World, logic description in understandable sequences.
 *
 */
function Мир(префикс = "")
{
    // Разобрать события и реакции, выраженные в тексте.
    this.разобрать = function(череда)
    {
        var соответствия = this._событияРеакции(череда);
        for (var событие in соответствия)
        {
            if (!(событие in this.события))
            {
                this.события[событие] = new Уведомитель();

                // Отладка.
                var тут = this;
                const название = событие;
                this.события[название].подписать(function() {
                    тут.отладить(`Соб. '${название}'`);
                });
            }
            var реакции = соответствия[событие];
            for (var номер in реакции)
            {
                const реакция = реакции[номер];
                const название = this._имяФункцииИзРеакции(реакция);
                const функция = eval(название);
                var тут = this;
                this.события[событие].подписать(function(){
                    // Отладка.
                    тут.отладить(`Реакция '${реакция}'`);

                    функция(тут);
                });
            }
        }
    };
    
    // Уведомить о событии при его наличии.
    this.уведомить = function(событие)
    {
        if (событие in this.события)
        {
            this.события[событие].уведомить();
        }
    };

    // Уведомить слушателей (при наличии) о сообщении отладки.
    this.отладить = function(сообщение)
    {
        this.сообщениеОтладки = сообщение;
        this.изменилиСообщениеОтладки.уведомить();
    }

    // Конструктор.
    this._создать = function()
    {
        this.события = {};
        this.настройки = {};

        this.сообщениеОтладки = null;
        this.изменилиСообщениеОтладки = new Уведомитель();
    };

    // Разобрать текст с событиями и реакциями, вернуть словарь их соответствия.
    this._событияРеакции = function(текст)
    {
        var соответствие = {};
        
        var элементы = текст.split("\n");
        var имяСобытия = null;
        for (var номер in элементы)
        {
            var элемент = элементы[номер];
            // Пропускаем комментарии.
            if (элемент.charAt(0) == "#")
            {
                continue;
            }
            var имя = элемент.trim();
            // Пропускаем пустые строки.
            if (!имя.length)
            {
                continue;
            }
            // Событие.
            if (имя == элемент)
            {
                if (!(имя in соответствие))
                {
                    имяСобытия = имя;
                    соответствие[имя] = [];
                }
            }
            // Реакция.
            else
            {
                соответствие[имяСобытия].push(имя);
            }
        }
        
        return соответствие;
    };

    // Преобразовать имя реакции в название функции.
    this._имяФункцииИзРеакции = function(реакция)
    {
        var имя = "";
        
        var части = реакция.split(" ");
        for (var номер in части)
        {
            var часть = части[номер];
            имя += часть.charAt(0).toUpperCase() + часть.slice(1);
        }
        
        return префикс + имя;
    };

    // Конструктор.
    this._создать();

    // English.
    // Parse events and reactions represented as text.
    this.parse = this.разобрать;
    // Notify about an event if it's present.
    this.report = this.уведомить;
    this.events = this.события;
    this.settings = this.настройки;
}


//MDRU/Классы/Модуль
//MDRU 
//MDRU Позволяет менять содержимое модуля GitJS:
//MDRU 
//MDRU * добавлять / удалять файл;
//MDRU * задавать файлу флаг `исполнить`;
//MDRU * изменять содержимое файла.
//MDRU 
//MDRU Экземпляр класса `Модуль` можно получить с помощью методов
//MDRU `модульПоИмени()` и `модульПоУказателю()` у экземпляра классса
//MDRU `Модули`, расположенного в `мир.модули`.
//MDRU 

//MDEN/Classes/Module
//MDEN 
//MDEN Allows you to change GitJS module contents:
//MDEN 
//MDEN * add / remove a file;
//MDEN * set `исполнить` (`execute`) flag;
//MDEN * change file contents.
//MDEN 
//MDEN An instance of `Module` class can be obtained with the help of
//MDEN `moduleByName()` and `moduleByPointer()` methods of `Modules` class
//MDEN instance located in `world.modules`.
//MDEN 

function Модуль(имя, версия, дом, указатель, http, структура)
{
    this.имя = имя;
    this.версия = версия;
    this["🏠"] = дом;
    this.указатель = указатель;
    this.http = http;
    this.структура = структура;
    this.содержимое = null;

    // НАДО Продублировать имена на инглише с помощью свойств объекта.

    // НАДО MDRU/EN

    this.исполнить = function()
    {
        const УКАЗАТЕЛЬ_ЭТОГО_МОДУЛЯ = this.указатель;
        var имена = Object.keys(this.структура).sort();
        for (var н in имена)
        {
            const ИМЯ_ЭТОГО_ФАЙЛА = имена[н];
            var свойства = this.структура[ИМЯ_ЭТОГО_ФАЙЛА];
            if (свойства.исполнить)
            {
                eval(this.содержимое[ИМЯ_ЭТОГО_ФАЙЛА]);
            }
        }
    };

    this.execute = this.исполнить;

    //mdru/Модуль/исполнитьФайл
    //mdru 
    //mdru Исполнить файл модуля руками.
    //mdru 

    this.исполнитьФайл = function(имя)
    {
        eval(this.содержимое[имя]);
    };

    //mden/Module/executeFile
    //mden 
    //mden Execute module's file manually.
    //mden 

    this.executeFile = this.исполнитьФайл;

    // НАДО MDRU/EN


    this.исполнитьЧерёды = function(мир)
    {
        // Собираем исполняемые файлы и черёды.
        var исполняемые = {};
        var черёды = [];
        var имена = Object.keys(this.структура).sort();
        for (var н in имена)
        {
            var имя = имена[н];
            var свойства = this.структура[имя];
            if (свойства.исполнить)
            {
                console.debug(`Модуль.исполнитьЧерёды имя: '${имя}'`);
                исполняемые[имя] = true;
            }
            else if (имя.endsWith(".череда"))
            {
                черёды.push(имя);
            }
        }
        // Исполняем черёды, соответствующие исполняемым файлам.
        for (var номер in черёды)
        {
            var череда = черёды[номер];
            var имяИсполняемогоФайла =
                череда.substring(0, череда.length - ".череда".length) + ".js";
            if (имяИсполняемогоФайла in исполняемые)
            {
                console.debug(`Модуль.исполнитьЧерёды разобрать: '${череда}'`);
                мир.разобрать(this.содержимое[череда]);
            }
        }
    };

    this.executeSequences = this.исполнитьЧерёды;

    //mdru/Модуль/исполнитьЧереду
    //mdru 
    //mdru Исполнить череду модуля руками.
    //mdru 

    this.исполнитьЧереду = function(мир, имя)
    {
        мир.разобрать(this.содержимое[имя]);
    };

    //mden/Module/executeSequence
    //mden 
    //mden Execute module's sequence manually.
    //mden 

    this.executeSequence = this.исполнитьЧереду;

}


//MDRU # Класс `Модули`
//MDRU 
//MDRU Позволяет управлять модулями GitJS:
//MDRU 
//MDRU * загружать модуль по сети (HTTP) или из кэша браузера (IndexedDB);
//MDRU * сохранять изменения модуля в кэш браузера.
//MDRU 
//MDRU Экземпляр класса `Модули` создаётся пусковым скриптом `0000.js`
//MDRU и доступен как `мир.модули`.
//MDRU 

//MDEN 
//MDEN # `Modules` class
//MDEN
//MDEN Allows you to manage GitJS modules:
//MDEN 
//MDEN * load a module over network (HTTP) or from the browser cache (IndexedDB);
//MDEN * save changes of a module into browser cache.
//MDEN 
//MDEN An instance of `Modules` class is created by `0000.js` startup script
//MDEN and is available as `world.modules`.
//MDEN 

function Модули(ключ, мир)
{
    //MDRU 
    //MDRU ## Метод `Модули.использовать(указатели)`
    //MDRU 
    //MDRU Делает следующее с предоставленным списком указателей на модули:
    //MDRU 
    //MDRU * проверяет наличие всех запрошенных модулей в кэше;
    //MDRU * загружает по сети (HTTP) отсутствующие в кэше модули;
    //MDRU * сохраняет в кэше загруженные по сети модули;
    //MDRU * исполняет с помощью `eval()` у каждого модуля файлы `.js` с флагом `исполнить`;
    //MDRU * исполняет у каждого модуля файлы `.череда` в случае наличия файла-близнеца с расширением `.js` и флагом `исполнить`.
    //MDRU 
    //MDRU Для отслеживания результата использования следует подписаться на следующие
    //MDRU экземпляры класса `Уведомитель`:
    //MDRU 
    //MDRU * `мир.модули.использовали` для отслеживания успешного использования;
    //MDRU * `мир.модули.неИспользовали` для отслеживания неудачного использования;
    //MDRU * `мир.модули.неСохранили` для отслеживания неудачного сохранения в кэш.
    //MDRU 
    //MDRU Пример:
    //MDRU 
    //MDRU ```js
    //MDRU мир.модули.использовать([
    //MDRU     "https://bitbucket.org/gitjs/privet-hello/raw/master/0000",
    //MDRU     "http://localhost:40000/gitjs/proverit-test-0000/0000",
    //MDRU ]);
    //MDRU 
    //MDRU // Успешное использование.
    //MDRU мир.модули.использовали.подписатьРаз(function() {
    //MDRU     console.debug("Модули использованы. Поехали!");
    //MDRU });
    //MDRU 
    //MDRU // Неудачное использование.
    //MDRU мир.модули.неИспользовали.подписатьРаз(function() {
    //MDRU     console.error(`ОШИБКА Не удалось использовать модули: '${мир.модули.ошибка}'`);
    //MDRU });
    //MDRU 
    //MDRU // Неудачное сохранение.
    //MDRU мир.модули.неСохранили.подписатьРаз(function() {
    //MDRU     console.error(`ОШИБКА Не удалось сохранить модули: '${мир.модули.ошибка}'`);
    //MDRU });
    //MDRU ```
    //MDRU 

    //MDEN 
    //MDEN ## `Modules.use(pointers)` method
    //MDEN 
    //MDEN Performs the following actions with the provided pointers to modules:
    //MDEN 
    //MDEN * looks up the modules in the cache;
    //MDEN * loads modules over network (HTTP) if they are not not present in the cache;
    //MDEN * saves loaded modules into the cache;
    //MDEN * executes each `.js` file having `исполнить` (`execute`) flag with the help of `eval()`;
    //MDEN * executes each `.череда` (sequence) file if it has a twin `.js` file with `исполнить` (`execute`) flag set.
    //MDEN 
    //MDEN To track the status of `use()` execution you should subscribe to one of
    //MDEN the following instances of `Reporter` class:
    //MDEN 
    //MDEN * `world.modules.used` to track successful usage;
    //MDEN * `world.modules.notUsed` to track failed usage;
    //MDEN * `world.modules.notSaved` to track failed caching.
    //MDEN 
    //MDEN Example:
    //MDEN 
    //MDEN ```js
    //MDEN world.modules.use([
    //MDEN     "https://bitbucket.org/gitjs/privet-hello/raw/master/0000",
    //MDEN     "http://localhost:40000/gitjs/proverit-test-0000/0000",
    //MDEN ]);
    //MDEN 
    //MDEN // Successful usage.
    //MDEN world.modules.used.subscribeOnce(function() {
    //MDEN     console.debug("Modules were used. Let's go!");
    //MDEN });
    //MDEN 
    //MDEN // Failed usage.
    //MDEN world.modules.notUsed.subscribeOnce(function() {
    //MDEN     console.error(`ERROR Could not use modules: '${world.modules.error}'`);
    //MDEN });
    //MDEN 
    //MDEN // Failed saving.
    //MDEN world.modules.notSaved.subscribeOnce(function() {
    //MDEN     console.error(`ERROR Could not save modules: '${world.modules.error}'`);
    //MDEN });
    //MDEN ```
    //MDEN 

    this.использовали = new мир.Уведомитель();
    this.неИспользовали = new мир.Уведомитель();
    this.неСохранили = new мир.Уведомитель();

    this.used = this.использовали;
    this.notUsed = this.неИспользовали;
    this.notSaved = this.неСохранили;

    this.использовать = function(указатели)
    {
        расшифрованныеУказатели = this._расшифроватьУказатели(указатели);
        this.исп.используемыеУказатели = расшифрованныеУказатели.slice();
        this.исп.указатели = расшифрованныеУказатели.slice();
        this.исп.уведомить("использовать");
        this.исп.тут = this;
    };

    this.use = this.использовать;


    // НАДО Описать MDRU/EN
    this.отладили = new мир.Уведомитель();
    this.отладка = null;

    this.debugged = this.отладили;
    this.debug = null;

    // НАДО Описать MDRU/EN
    this.модульПоИмени = function(имя)
    {
        for (var указатель in this.модули)
        {
            var модуль = this.модули[указатель];
            if (модуль.имя == имя)
            {
                return модуль;
            }
        }

        return null;
    };
    this.moduleByName = this.модульПоИмени;

    // НАДО Описать MDRU/EN
    this.модульПоУказателю = function(указатель)
    {
        return this.модули[указатель];
    };
    this.moduleByPointer = this.модульПоУказателю;

    //mdru/Модули/загрузилиСодержимоеСохранённогоМодуля
    //mdru 
    //mdru Уведомление об успешном исполнении метода
    //mdru `загрузитьСодержимоеСохранённогоМодуля`.
    //mdru 
    this.загрузилиСодержимоеСохранённогоМодуля = new мир.Уведомитель();

    //mden/Modules/loadedSavedModuleContents
    //mden 
    //mden Notification of a successful completion of
    //mden `loadedSavedModuleContents` method.
    //mden 
    this.loadedSavedModuleContents = this.загрузилиСодержимоеСохранённогоМодуля;

    //mdru/Модули/неЗагрузилиСодержимоеСохранённогоМодуля
    //mdru 
    //mdru Уведомление о неудачном исполнении метода
    //mdru `загрузитьСодержимоеСохранённогоМодуля`.
    //mdru 
    this.неЗагрузилиСодержимоеСохранённогоМодуля = new мир.Уведомитель();

    //mden/Modules/notLoadedSavedModuleContents
    //mden 
    //mden Notification of a failed attempt to execute
    //mden `loadedSavedModuleContents` method.
    //mden 
    this.notLoadedSavedModuleContents = this.неЗагрузилиСодержимоеСохранённогоМодуля;

    //mdru/Модули/загрузитьСодержимоеСохранённогоМодуля
    //mdru 
    //mdru Загружает содержимое сохранённого модуля по указателю.
    //mdru 
    this.загрузитьСодержимоеСохранённогоМодуля = function(указатель)
    {
        var модуль = this.модульПоУказателю(указатель);
        if (!модуль)
        {
            this.ошибка = "ОШИБКА Сохранённого модуля с таким указателем не существует | ERROR There is no saved module under such pointer"; 
            this.error = this.ошибка;
            this.неЗагрузилиСодержимоеСохранённогоМодуля.уведомить();
            return;
        }

        var тут = this;
        localforage.getItem(
            указатель,
            function(ошибка, содержимое)
            {
                if (содержимое)
                {
                    модуль.содержимое = содержимое;
                    тут.загрузилиСодержимоеСохранённогоМодуля.уведомить();
                }
                else
                {
                    this.ошибка = ошибка;
                    this.error = this.ошибка;
                    тут.неЗагрузилиСодержимоеСохранённогоМодуля.уведомить();
                }
            }
        );
    };

    //mden/Modules/loadSavedModuleContents
    //mden 
    //mden Load saved module contents by pointer.
    //mden
    this.loadSavedModuleContents = this.загрузитьСодержимоеСохранённогоМодуля;

    //mdru/Модули/сохранили
    //mdru 
    //mdru Уведомление об успешном исполнении метода `сохранить`.
    //mdru 
    this.сохранили = new мир.Уведомитель();

    //mden/Modules/saved
    //mden 
    //mden Notification of a successful completion of `save` method.
    //mden 
    this.saved = this.сохранили;

    //mdru/Модули/неСохранили
    //mdru 
    //mdru Уведомление о неудачном исполнении метода `сохранить`.
    //mdru 
    this.неСохранили = new мир.Уведомитель();

    //mden/Modules/notSaved
    //mden 
    //mden Notification of a failed attempt to execute `save` method.
    //mden 
    this.notSaved = this.неСохранили;

    //mdru/Модули/сохранить
    //mdru 
    //mdru Сохраняет модуль в хранилище браузера.
    //mdru 
    this.сохранить = function(указатель)
    {
        var модуль = this.модули[указатель];
        var тут = this;
        // Сначала кэшируем в IndexedDB, а затем в LocalStorage.
        // First cache into IndexedDB, then to LocalStorage.
        localforage.setItem(
            указатель,
            модуль.содержимое,
            function(ошибка, значение) {
                if (ошибка)
                {
                    тут.ошибка = ошибка;
                    тут.error = ошибка;
                    тут.неСохранили.уведомить();
                }
                else
                {
                    var описание = {
                        "имя": модуль.имя,
                        "версия": модуль.версия,
                        "🏠": модуль["🏠"],
                        "структура": модуль.структура,
                    };
                    localStorage.setItem(указатель, JSON.stringify(описание));
                    тут.сохранили.уведомить();
                }
            }
        );
    };

    //mden/Modules/save
    //mden 
    //mden Save module into browser storage.
    //mden
    this.save = this.сохранить;

    this._создать(ключ, мир);

}


// Частные функции класса `Модули`.
// Private functions of `Modules` class.


Модули.prototype._создать = function(ключ, мир)
{
    localforage.config({ name: ключ });
    this.модули = {};
    this._загрузитьОписаниеСохранённыхМодулей();
    this._создатьМирИспользования(мир);
};


// // // //


Модули.prototype._расшифроватьУказатели = function(указатели)
{
    var расшифровка = [];
    for (var номер in указатели)
    {
        var указатель = указатели[номер];
        расшифровка.push(decodeURI(указатель));
    }
    return расшифровка;
};


// // // //


Модули.prototype._загрузитьОписаниеСохранённыхМодулей = function()
{
    var указатели = [];
    for (var номер = 0; номер < localStorage.length; ++номер)
    {
        var ключ = localStorage.key(номер);
        if (ключ.startsWith("http"))
        {
            указатели.push(ключ);
        }
    }

    for (var номер in указатели)
    {
        var указатель = указатели[номер];
        var представление = localStorage.getItem(указатель);
        if (представление)
        {
            var описание = JSON.parse(представление);
            var модуль =
                new Модуль(
                    описание.имя, 
                    описание.версия,
                    описание["🏠"],
                    указатель,
                    указатель + "/..",
                    описание.структура
                );
            this.модули[указатель] = модуль;
        }
    }
};


// // // //


Модули.prototype._создатьМирИспользования = function(мир)
{
    this.исп = new мир.Мир();

    this.исп.мир = мир;
    this.исп.модули = this.модули;
    this.исп.использовали = this.использовали;
    this.исп.неИспользовали = this.неИспользовали;
    this.исп.неСохранили = this.неСохранили;

    var тут = this;
    this.исп.изменилиСообщениеОтладки.подписать(function() {
        тут.отладка = тут.исп.сообщениеОтладки;
        тут.debug = тут.отладка;
        тут.отладили.уведомить();
    });

    this.исп.разобрать(`
использовать
    загрузить следующий модуль
проверить наличие модуля в кэше
    загрузить содержимое модуля из кэша
загрузили содержимое модуля из кэша
    загрузить следующий модуль
не загрузили содержимое модуля из кэша
    загрузить описание модуля из сети
ошибка загрузки
    уведомить об ошибке использования
загрузили описание модуля из сети
    загрузить содержимое модуля из сети
загрузить файлы модуля из сети
    загрузить следующий файл
загрузили файл
    загрузить следующий файл
загрузили файлы
    загрузить следующий модуль
загрузили модули
    исполнить файлы модулей
    исполнить черёды модулей
    сохранить загруженные модули
    уведомить об успешном использовании
ошибка сохранения
    уведомить об ошибке сохранения
    `);
};


// Реакции для миров класса `Модули`.
// Reactions for the worlds of `Modules` class.


function ЗагрузитьСледующийМодуль(мир)
{
    мир.указатель = мир.указатели.shift()

    мир.отладить(`ЗагрузитьСледующийМодуль указатель: '${мир.указатель}'`);

    if (мир.указатель)
    {
        мир.уведомить("проверить наличие модуля в кэше")
    }
    else
    {
        мир.уведомить("загрузили модули");
    }
}


// // // //


function ЗагрузитьСодержимоеМодуляИзКэша(мир)
{
    мир.отладить(`ЗагрузитьСодержимоеМодуляИзКэша. указатель: '${мир.указатель}'`);

    var модуль = мир.модули[мир.указатель];
    if (модуль)
    {
        localforage.getItem(
            мир.указатель,
            function(ошибка, содержимое)
            {
                if (содержимое)
                {
                    модуль.содержимое = содержимое;
                    мир.уведомить("загрузили содержимое модуля из кэша");
                }
                else
                {
                    мир.уведомить("не загрузили содержимое модуля из кэша");
                }
            }
        );
    }
    else
    {
        мир.уведомить("не загрузили содержимое модуля из кэша");
    }
}


// // // //


function ЗагрузитьОписаниеМодуляИзСети(мир)
{
    мир.мир.взять(
        мир.указатель,
        function(ответ)
        {
            var описание = разобрать0000(ответ);
            var модуль =
                new Модуль(
                    описание.имя,
                    описание.версия,
                    описание["🏠"],
                    мир.указатель,
                    мир.указатель + "/..",
                    описание.структура
                );
            мир.модули[мир.указатель] = модуль;
            мир.модуль = модуль;
            мир.уведомить("загрузили описание модуля из сети");
        },
        function(ошибка)
        {
            мир.тут.ошибка = ошибка;
            мир.тут.error = ошибка;
            мир.уведомить("ошибка загрузки");
        }
    );
}


// // // //


function УведомитьОбОшибкеИспользования(мир)
{
    мир.неИспользовали.уведомить();
}


// // // //


function ЗагрузитьСодержимоеМодуляИзСети(мир)
{
    мир.файлы = [];
    for (var имя in мир.модуль.структура)
    {
        var свойства = мир.модуль.структура[имя];
        var адрес = мир.модуль.http + имя;
        var файл = {
            "имя": имя,
            "адрес": адрес,
            "двоичный": свойства.двоичный,
        };
        мир.файлы.push(файл);
    };
    мир.уведомить("загрузить файлы модуля из сети");
}


// // // //


function ЗагрузитьСледующийФайл(мир)
{
    мир.файл = мир.файлы.shift()

    мир.отладить(`ЗагрузитьСледующийФайл. файл: '${JSON.stringify(мир.файл)}'`);

    if (мир.файл)
    {
        мир.мир.взять(
            мир.файл.адрес,
            function(содержимое)
            {
                if (!мир.модуль.содержимое)
                {
                    мир.модуль.содержимое = {};
                }

                мир.модуль.содержимое[мир.файл.имя] = содержимое;
                мир.уведомить("загрузили файл");
            },
            function(ошибка)
            {
                мир.тут.ошибка = ошибка;
                мир.тут.error = ошибка;
                мир.уведомить("ошибка загрузки");
            },
            мир.файл.двоичный
        );
    }
    else
    {
        мир.уведомить("загрузили файлы");
    }
}


// // // //


function СохранитьЗагруженныеМодули(мир)
{
    for (var номер in мир.используемыеУказатели)
    {
        const указатель = мир.используемыеУказатели[номер];
        мир.отладить(`СохранитьЗагруженныеМодули. указатель: '${указатель}'`);
        const модуль = мир.модули[указатель];
        // Сначала кэшируем в IndexedDB, а затем в LocalStorage.
        // First cache into IndexedDB, then to LocalStorage.
        localforage.setItem(
            указатель,
            модуль.содержимое,
            function(ошибка, значение) {
                if (ошибка)
                {
                    мир.тут.ошибка = ошибка;
                    мир.тут.error = ошибка;
                    мир.уведомить("ошибка сохранения");
                }
                else
                {
                    var описание = {
                        "имя": модуль.имя,
                        "версия": модуль.версия,
                        "🏠": модуль["🏠"],
                        "структура": модуль.структура,
                    };
                    localStorage.setItem(указатель, JSON.stringify(описание));
                }
            }
        );
    }
}


// // // //


function ИсполнитьФайлыМодулей(мир)
{
    for (var номер in мир.используемыеУказатели)
    {
        var указатель = мир.используемыеУказатели[номер];
        мир.отладить(`ИсполнитьФайлыМодулей. указатель: '${указатель}'`);
        var модуль = мир.модули[указатель];
        модуль.исполнить();
    }
}


// // // //


function ИсполнитьЧерёдыМодулей(мир)
{
    for (var номер in мир.используемыеУказатели)
    {
        var указатель = мир.используемыеУказатели[номер];
        мир.отладить(`ИсполнитьЧерёдыМодулей. указатель: '${указатель}'`);
        var модуль = мир.модули[указатель];
        модуль.исполнитьЧерёды(мир.мир);
    }
}


// // // //


function УведомитьОбУспешномИспользовании(мир)
{
    мир.использовали.уведомить();
}


// // // //


function УведомитьОбОшибкеСохранения(мир)
{
    мир.неСохранили.уведомить();
}



// Точка входа.
// Entry point.
function запуститьGitJS()
{
    gitjs.мир = new Мир();
    var мир = gitjs.мир;
    мир.взять = взять;
    мир.разобрать0000 = разобрать0000;
    мир.собрать0000 = собрать0000;
    мир.сделатьПоследовательно = сделатьПоследовательно;
    мир.сделатьПараллельно = сделатьПараллельно;
    мир.Мир = Мир;
    мир.Уведомитель = Уведомитель;
    мир.модули = new Модули("gitjs", мир);

    мир.uuid = gitjs.uuid;

    gitjs.world = мир;
    var world = мир;
    world.get = взять;
    world.parse0000 = разобрать0000;
    world.assemble0000 = собрать0000;
    world.doSequentially = сделатьПоследовательно;
    world.doInParallel = сделатьПараллельно;
    world.World = Мир;
    world.Reporter = Уведомитель;
    world.modules = мир.модули;

    var указатель = "https://git.opengamestudio.org/BCE/BCE/raw/branch/master/0000";
    
    var запрос = window.location.search.substring(1);
    if (запрос.startsWith("http"))
    {
        var конец = запрос.indexOf("/0000");
        if (конец != -1)
        {
            указатель = запрос.substring(0, конец + 5);
        }
    }

    указатель = decodeURI(указатель);

    // Выводить в консоль отладочные сообщения Модулей.
    мир.модули.отладили.подписать(function() {
        console.debug(мир.модули.отладка);
    });

    мир.модули.использовали.подписатьРаз(function() {
        console.debug("Использовали. Запускаем...");
        мир.уведомить("пуск");
    });
    мир.модули.неИспользовали.подписатьРаз(function() {
        var сообщение = `ОШИБКА Не удалось использовать модули | ERROR Could not use modules '${мир.модули.ошибка}'`;
        console.error(сообщение);
        document.body.innerHTML += `<p>${сообщение}</p>`;
    });
    мир.модули.неСохранили.подписатьРаз(function() {
        var сообщение = `ОШИБКА Не удалось сохранить модули в кэше | ERROR Could not cache modules '${мир.модули.ошибка}'`;
        console.error(сообщение);
    });

    var сообщение = "Загрузка модуля ⚬ 模块加载 ⚬ Loading module";
    console.debug(сообщение);
    console.debug(указатель);

    мир.модули.использовать([указатель]);
}
