
//MDRU/Классы/Модуль
//MDRU 
//MDRU Позволяет менять содержимое модуля GitJS:
//MDRU 
//MDRU * добавлять / удалять файл;
//MDRU * задавать файлу флаг `исполнить`;
//MDRU * изменять содержимое файла.
//MDRU 
//MDRU Экземпляр класса `Модуль` можно получить с помощью методов
//MDRU `модульПоИмени()` и `модульПоУказателю()` у экземпляра классса
//MDRU `Модули`, расположенного в `мир.модули`.
//MDRU 

//MDEN/Classes/Module
//MDEN 
//MDEN Allows you to change GitJS module contents:
//MDEN 
//MDEN * add / remove a file;
//MDEN * set `исполнить` (`execute`) flag;
//MDEN * change file contents.
//MDEN 
//MDEN An instance of `Module` class can be obtained with the help of
//MDEN `moduleByName()` and `moduleByPointer()` methods of `Modules` class
//MDEN instance located in `world.modules`.
//MDEN 

function Модуль(имя, версия, дом, указатель, http, структура)
{
    this.имя = имя;
    this.версия = версия;
    this["🏠"] = дом;
    this.указатель = указатель;
    this.http = http;
    this.структура = структура;
    this.содержимое = null;

    // НАДО Продублировать имена на инглише с помощью свойств объекта.

    // НАДО MDRU/EN

    this.исполнить = function()
    {
        const УКАЗАТЕЛЬ_ЭТОГО_МОДУЛЯ = this.указатель;
        var имена = Object.keys(this.структура).sort();
        for (var н in имена)
        {
            const ИМЯ_ЭТОГО_ФАЙЛА = имена[н];
            var свойства = this.структура[ИМЯ_ЭТОГО_ФАЙЛА];
            if (свойства.исполнить)
            {
                eval(this.содержимое[ИМЯ_ЭТОГО_ФАЙЛА]);
            }
        }
    };

    this.execute = this.исполнить;

    //mdru/Модуль/исполнитьФайл
    //mdru 
    //mdru Исполнить файл модуля руками.
    //mdru 

    this.исполнитьФайл = function(имя)
    {
        eval(this.содержимое[имя]);
    };

    //mden/Module/executeFile
    //mden 
    //mden Execute module's file manually.
    //mden 

    this.executeFile = this.исполнитьФайл;

    // НАДО MDRU/EN


    this.исполнитьЧерёды = function(мир)
    {
        // Собираем исполняемые файлы и черёды.
        var исполняемые = {};
        var черёды = [];
        var имена = Object.keys(this.структура).sort();
        for (var н in имена)
        {
            var имя = имена[н];
            var свойства = this.структура[имя];
            if (свойства.исполнить)
            {
                console.debug(`Модуль.исполнитьЧерёды имя: '${имя}'`);
                исполняемые[имя] = true;
            }
            else if (имя.endsWith(".череда"))
            {
                черёды.push(имя);
            }
        }
        // Исполняем черёды, соответствующие исполняемым файлам.
        for (var номер in черёды)
        {
            var череда = черёды[номер];
            var имяИсполняемогоФайла =
                череда.substring(0, череда.length - ".череда".length) + ".js";
            if (имяИсполняемогоФайла in исполняемые)
            {
                console.debug(`Модуль.исполнитьЧерёды разобрать: '${череда}'`);
                мир.разобрать(this.содержимое[череда]);
            }
        }
    };

    this.executeSequences = this.исполнитьЧерёды;

    //mdru/Модуль/исполнитьЧереду
    //mdru 
    //mdru Исполнить череду модуля руками.
    //mdru 

    this.исполнитьЧереду = function(мир, имя)
    {
        мир.разобрать(this.содержимое[имя]);
    };

    //mden/Module/executeSequence
    //mden 
    //mden Execute module's sequence manually.
    //mden 

    this.executeSequence = this.исполнитьЧереду;

}

