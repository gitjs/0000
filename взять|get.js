/*
 *
 * Получение данных по HTTP(S).
 * Getting data over HTTP(S).
 *
 */
function взять(адрес, откликУспех, откликПровал = null, двоичные = null)
{
    var запрос = new XMLHttpRequest();
    if (двоичные)
    {
        запрос.responseType = "arraybuffer";
    }
    запрос.onreadystatechange = function()
    {
        if (this.readyState == 4)
        {
            if (this.status == 200)
            {
                откликУспех(двоичные ? this.response : this.responseText);
            }
            else if (откликПровал)
            {
                откликПровал(this.status);
            }
        }
    }
    запрос.open("GET", адрес + "?" + gitjs.uuid());
    запрос.send();
}
